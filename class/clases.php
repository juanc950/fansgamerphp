<?php
require_once("class.php");

class Socialnet{

	private $usuario;
	private $amigos;
	private $notificacion;

	public function __construct(){
		$this->usuario = array();
		$this->amigos = array();
		$this->notificacion = array();
	}

	//metodo para obtener los datos del usuario logueado
	public function datos_usuario()
	{
		$sql="SELECT CONCAT(nombres,' ', apellidos) nombre, correo, fechaNac, foto, verCorreo, verFechaNac FROM usuarios WHERE idUsuario = ".$_SESSION["sesion_id"];
		$res=mysql_query($sql,Conectar::con());
		while ($reg=mysql_fetch_assoc($res))
		{
			$this->usuario[]=$reg;
		}
			return $this->usuario;
	}

	//metodo para obtener los datos de amigo
	public function datos_amigo( $id )
	{
		$sql="SELECT CONCAT(nombres,' ', apellidos) nombre, correo, fechaNac, foto, verCorreo, verFechaNac FROM usuarios WHERE idUsuario = ".$id;
		$res=mysql_query($sql,Conectar::con());
		while ($reg=mysql_fetch_assoc($res))
		{
			$this->usuario[]=$reg;
		}
			return $this->usuario;
	}

	//metodo para ver las notificaciones
	public function ver_notificaciones(){
		$sql = " SELECT u.idUsuario, CONCAT(u.nombres,' ',u.apellidos) as nombre, if(n.tipoNotificacion = 1,'solicitud','comentario') as notificacion, n.fecha, n.idNotificacion "
			." FROM notificaciones n "
			." INNER JOIN usuarios u ON n.idUsuarioEnvia = u.idUsuario "
			." WHERE n.idUsuarioEnvia = u.idUsuario "
			." AND "
			." n.idUsuarioRecibe = '".$_SESSION["sesion_id"]."' "
			." ORDER BY n.fecha DESC";
		$res=mysql_query($sql,Conectar::con());

		while ($reg=mysql_fetch_assoc($res))
		{
			$this->notificacion[]=$reg;
		}
			return $this->notificacion;
	}

	//metodo para obtener todos los amigos del usuario logueado
	public function amigos( $id ){
		$sql = "select concat(u.nombres,' ',u.apellidos) as nombre, u.foto, u.idUsuario 
				from usuarios u
				inner join amigos a on a.idUsuarioAmigo = u.idUsuario
				where a.idUsuario = '".$id."' order by nombre asc ";

		$res=mysql_query($sql,Conectar::con());

		while ($reg=mysql_fetch_assoc($res))
		{
			$this->amigos[]=$reg;
		}
			return $this->amigos;


	}

	//metodo para saber si es amigo 
	public function amigo( $id ){
		$sql = "SELECT * FROM amigos WHERE idUsuario = '".$_SESSION['sesion_id']."' AND idUsuarioAmigo = '".$id."' ";
		$res = mysql_query( $sql, Conectar::con() );
		$cant = mysql_num_rows( $res );		

		if( $cant == 1 ){
			return 1; // retorna 1 si es amigo
		}else{
			$sql = "SELECT * FROM notificaciones WHERE idUsuarioRecibe = '".$id."' AND idUsuarioEnvia = '".$_SESSION['sesion_id']."' AND tipoNotificacion = 1 ";
			$res = mysql_query( $sql, Conectar::con() );
			$cant = mysql_num_rows( $res );

			if( $cant == 0 ){
				return 0; // retorna 0 si aun no es amigo
			}else{
				return 2; // retorna 2 si ya se envio una solicitud
			}
		}
	}

	//metodo para obtener las notificaciones del usuario logueado
	public function notificaciones()
	{
		$sql="SELECT * FROM notificaciones WHERE idUsuarioRecibe = ".$_SESSION["sesion_id"];
		
		$res=mysql_query($sql,Conectar::con());

		$cant = mysql_num_rows( $res );

		return $cant;		
	}

}

?>