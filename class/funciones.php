<?php

// funcion que trunca caracteres, se debe mandar el texto $texto y el numero de caracteres que se quieren mostrar $limite
function truncarTexto($texto,$limite){
	$rest = substr ( $texto , 0 , $limite );
	return $rest;
}

// funcion que cambia a formato para MySQL (AÑO-MES-DIA)
function cambiafechamysql($fecha){ 
ereg( "([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})", $fecha, $mifecha); 
$fechana=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
return $fechana;
}

// funcion que cambia a formato normal (DIA-MES-AÑO)
function cambiafechanormal($fecha){ 
ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha); 
$lafecha = $mifecha[3] . "-" . $mifecha[2] . "-" . $mifecha[1]; 
return $lafecha;
}

function tilde_html($entra)
{
	$traduce=array( 'á' => '&aacute;' , 'é' => '&eacute;' , 'í' => '&iacute;' , 'ó' => '&oacute;' , 'ú' => '&uacute;' , 'ñ' => '&ntilde;' , 'Ñ' => '&Ntilde;' , 'ä' => '&auml;' , 'ë' => '&euml;' , 'ï' => '&iuml;' , 'ö' => '&ouml;' , 'ü' => '&uuml;');
	$sale=strtr( $entra , $traduce );
	return $sale;
}

function chao_tilde($entra)
{
	$traduce=array( 'á' => 'a' , 'é' => 'e' , 'í' => 'i' , 'ó' => 'o' , 'ú' => 'u' , 'ñ' => 'n' , 'Ñ' => 'n' , 'ä' => 'a' , 'ë' => 'e' , 'ï' => 'i' , 'ö' => 'o' , 'ü' => 'u');
	$sale=strtr( $entra , $traduce );
	return $sale;
}


?>