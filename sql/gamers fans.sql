CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fechaNac` date NOT NULL,
  `foto` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activado` tinyint(1) DEFAULT NULL,
  `verCorreo` tinyint(1) NOT NULL DEFAULT '1',
  `verFechaNac` tinyint(1) NOT NULL DEFAULT '1',
  `funcion` int NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

create table funcion
(
codigo int primary key,
funcion varchar(50)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

create table listagamers
(
codigo int (11) not null primary key auto_increment,
nombre varchar(50) not null,
imagen varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
genero varchar(100) not null,
descripcicon varchar(1500) not null,
caracteristicas varchar(1500) not null,
numjugadores varchar(50) not null,
consolauso varchar(60) not null,
fechaemision varchar(30) not null
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

create table topgamers
(
codigo int (11) not null primary key auto_increment,
nombre varchar(50) not null,
imagen varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
genero varchar(100) not null,
descripcicon varchar(1500) not null,
caracteristicas varchar(1500) not null,
numjugadores varchar(50) not null,
consolauso varchar(60) not null,
fechaemision varchar(30) not null
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

create table consolas
(
codigo int (11) not null primary key auto_increment,
nombre varchar(50) not null,
imagen varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
descripcicon varchar(1500) not null,
caracteristicas varchar(1500) not null,
controles varchar(50) not null,
empresa varchar(50) not null,
fechaemision varchar(30) not null
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;


create table comentario
(
codigo int (11) not null primary key auto_increment,
listagamers int(11),
topgamers int(11),
consolas int(11),
usuario int (11),
comentario varchar(1500) not null
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

INSERT INTO `usuarios`
(`idUsuario`,`nombres`,`apellidos`,`correo`,`contrasena`,`fechaNac`,`foto`,`activado`,`verCorreo`,`verFechaNac`,`funcion`)
values
(1,'Juan Carlos','Villatoro Sologaistoa','admin@admin.com','9dbf7c1488382487931d10235fc84a74bff5d2f4','1988-06-13','default.png',0,1,1,1);

insert into `funcion`
(`codigo`,`funcion`)
values
(1,'administrador'),
(2,'cliente');

alter table usuarios
add constraint funciontnes_fky 
foreign key (funcion) references funcion (codigo)

