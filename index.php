<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Home</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
      <!--<link type="text/css" href="css/jquery-ui-1.8.1.custom.css" rel="Stylesheet" />      -->
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <!--<script src="js/vendor/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.1.custom.min.js"></script>-->
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>      
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>      
      
      <script type="text/javascript">

      function ajax()
	  {        
        var form =document.formLogin;
        if ( form.correo.value != '' && form.pass.value != '')
		{
        var b = jQuery.noConflict();
        var url = "respuesta_login.php";
        b.ajax
		(
		{
          url: url,
          data:{"valor1":b("#correo").val(),"valor2":b("#pass").val()},
          contentType:"application/x-www-form-urlencoded",
          dataType:"html",
          error: function()
		  {
            alert("Error");
          },
          ifModified:false,
          processData:true,
		  
          success:function(datas)
		  {
            var resp = datas;         

            if( resp == 1 )
			{                            
              document.getElementById("correo").style.boxShadow = "5px 5px 2.2em blue";
              document.getElementById("pass").style.boxShadow = "5px 5px 2.2em blue";
              var segundos = 2000;              
              setTimeout('redireccion()',segundos);                           
            }
			
			if( resp == 2)
			{
              document.getElementById("correo").style.boxShadow = "5px 5px 2.2em blue";
              document.getElementById("pass").style.boxShadow = "5px 5px 2.2em blue";
              var segundos = 2000;              
              setTimeout('redireccion2()',segundos);				
			}
            if( resp ==0)
			{                           
              var segundos = 2000;                                          
              document.getElementById("correo").style.boxShadow = "5px 5px 2.2em red";
              document.getElementById("pass").style.boxShadow = "5px 5px 2.2em red";
			  
            }           
          },
          type: "POST",
          timeout:3000
        }
		);
        }
		
		<!--fin de la funcion if que valua si el espacio esta vacio y verifica los datos a ver
		else 
		{            
			document.getElementById("correo").style.boxShadow = "5px 5px 2.2em red";
            document.getElementById("pass").style.boxShadow = "5px 5px 2.2em red";
        }
		
		
      }
	  
      function redireccion()
	  {
        var pagina = 'clienListaGamers.php';
        document.location.href=pagina;
      }
	  function redireccion2()
	  {
		  var pagina = 'adminListaGamers.php';
		  document.location.href=pagina;
	  }
	 

    </script>      

      <script type="text/javascript">
        var a = jQuery.noConflict();            
        a(document).ready(function(){
          a(".boton").click(function(){
            a('#red_registro').dialog({  
              modal: true,            
              title: "Registro",
              width: 550,
              height: 370,
              minWidth: 400,
              maxWidth: 750,
              show: "bounce",
              hide: "blind",
              resizable: false  
            });
          });        
        });
      </script>

      <script>
        function validar(obj){
          var d = document.formRegistro;
          if(obj.checked==true){
            d.registrarse.disabled = false;            
          }else{
            d.registrarse.disabled = true;            
          }
        }
      </script>

      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>

   </head>
   <body>
    	<div id="red_content">
        	<?php include("header.php");?>
         <div id="red_page">
          <div id="red_space" style="height:1px; width:960px;"></div>
            <div id="red_page_unete" class="centrar">
            <p>  
            <div id="red_video">           
<param name="movie" value="//www.youtube-nocookie.com/embed/YTSNmhWigJY?rel=0"></param><embed src="//www.youtube-nocookie.com/embed/YTSNmhWigJY?rel=0"width="532" height="300"></embed>
</video>
</div>
             </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><img src="img/gamer.jpg" width="208" height="163" align="left" hspace="10"/>No esperas más y únete a los fans de gamer <span><a href="javascript:void()" class="boton">REGISTRATE</a></span>, únete 
                  y forma parte de esta comunidad aprende trucos y conoce los avances de los nuevos juegos                
              </p>
              <p>
                FANSGAMER.com una nueva forma de estar informado de los nuevos juegos de consolas y conocer amigos.
              </p>
            </div>

            <div id="red_registro" style="display:none;">

              <?php include("form_modal.php"); ?>
            </div>


         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>          

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'img/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
