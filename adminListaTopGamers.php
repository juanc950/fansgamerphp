<?php
require_once('class/clases.php');
require_once('class/funciones.php');

if( isset($_SESSION["sesion_usuario"]) && isset($_SESSION["sesion_id"]))
{  
  $objUsuario = new Socialnet();
  $reg = $objUsuario->datos_usuario();
  $notificaciones = $objUsuario->notificaciones();  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Mi Perfil</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
      <!--<link type="text/css" href="css/jquery-ui-1.8.1.custom.css" rel="Stylesheet" />      -->
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <!--<script src="js/vendor/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.1.custom.min.js"></script>-->
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
      <script src="js/buscar.js"></script>    
           <script type="text/javascript" src="adminajaxTop.js"></script> 

      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>      

   </head>
   <body>
    	<div id="red_content">
        	<?php include("adminheader.php");?>
          <div id="red_space" style="height:10px; width:950px;"></div>

         <div id="red_page" class="centrar">
            <div id="red_space" style="height:10px; width:960px;"></div>

            <div id="red_usuario_datos" class="centrar">
              <div id="red_usuario_nombre">
                <?php echo $reg[0]['nombre'];?>
              </div>
             <div id="DisADMINISTRADOR">
                <p>ADMINISTRADOR </p>
            </div>
            </div>
            
            <div id="cajon1"><li ><a href="adminListaGamers.php"><img src="img/perfil.png"> LISTA DE GAMERS</a></li>
                </div> 
               
                
           <div id="cajon2"><li class="red_lateral_activo"><a href="javascript:void()"><img src="img/logo_16.png"> TOP 10 GAMERS</a></li>
         </div>
         
         <div id="cajon3"><li><a href="adminConsolas.php"><img src="img/logo_16.png"> TIPOS DE CONSOLAS</a></li>
         </div>
         
         <div id="cajon4"><li><a href="adminHistoriaGamers.php"><img src="img/logo_16.png"> HISTORIA DEL GAMERS</a></li>
         </div>
         
           <div id = "red_deregistro" align="center">
 
<section id= "main">


	<article class="module width_full">
    	<header style="cursor:pointer">
        <h3>REGISTRO DE TOP 10 DEL MOMENTO</h3>
        </header>
        <div class="module_content">
        <form name="registrarImagen" action="adminListaTopGamers.php" method="post" enctype="multipart/form-data">

        <fieldset style="width:96%;">
        <label>NOMBRE </label>
        <input type="text" name="txtNombre" style="width:220px;"placeholder="DEL JUEGO EN MAYUSCULA" pattern="([a-zA-Z0-9]|\s)*"
            oninvalid="setCustomValidity('falta')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
        </fieldset>

        	<fieldset style="width:96%;">
            <label>FOTO A SUBIR </label>
            <input type="file" name="flsImagen" style="width:400px;" required>
            </fieldset>

             <fieldset style="width:96%;">
            <label>GENERO </label>
        <input type="text" name="txtgenero" style="width:220px;" placeholder="LETRAS EN MAYUSCULA" pattern="([a-zA-Z]|\s)*"
            oninvalid="setCustomValidity('falta')"
            onchange="try{setCustomValidity('')}catch(e){}"required>
        	</fieldset>
            
            <fieldset style="width:96%;">
            <label>DESCRIPCCION<br />
            </label>
            <textarea type="text" name="descripcion" cols="65" rows="5"pattern=	
            "^[A-Za-z0-9_]+" 
            oninvalid="setCustomValidity('falta error documentacion')"
            onchange="try{setCustomValidity('')}catch(e){}" required> 
            </textarea><br />
            </fieldset>
            
            <fieldset style="width:96%;">
            <label>CARACTERISTICAS<br />
            </label>
            <textarea type="text" name="caracteristicas" cols="65" rows="5" pattern=
            "^[A-Za-z0-9_]+" 
            oninvalid="setCustomValidity('falta error documentacion')"
            onchange="try{setCustomValidity('')}catch(e){}" requiere="required"> 
            </textarea><br />
            </fieldset>
         	
            <fieldset style="width:96%;">
            <label>CANTIDAD DE JUGADORES </label>
        <input type="text" name="txtnumerogamer" style="width:220px;" pattern="([0-9]|\s)*"
            oninvalid="setCustomValidity('falta')"
            onchange="try{setCustomValidity('')}catch(e){}"required>
        	</fieldset>
            
            <fieldset style="width:96%;">
            <label>PARA LA CONSOLA </label>
        <input type="text" name="txtconsola" style="width:220px;" placeholder="XBOX / PLAYSTATION" pattern="^([a-zA-Z]|\s)*"
            oninvalid="setCustomValidity('falta')"
            onchange="try{setCustomValidity('')}catch(e){}"required>
        	</fieldset>
            
            <fieldset style="width:96%">
            <label>FECHA DE EMISION </label>
            <input type="text" class="redondear" name="fecha" id="fecha" placeholder="DD-MM-YYYY"
            pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" 
            oninvalid="setCustomValidity('Fecha no válida')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
            </fieldset>
            
            <div class="clear"></div>
            
            <footer><div class="submit_link" style="float:left;padding:6px 0 0 10px;color:#666666"></div><div class="submit_link">
            <input name="btnGuardars" type="submit" value="Guardar" class="alt_btn">
            </div></footer>
            </form>
    </article>
</section>
 </div>
 <!-- lenguaje de conmunicacion con la tabla topgamer -->
           <?PHP
if (isset($_POST['btnGuardars'])){
	$archivo = $_FILES['flsImagen']['tmp_name'];
	$destino = "imagenes_listatop/". $_FILES['flsImagen']['name'];
	move_uploaded_file($archivo,$destino);
	mysql_query("INSERT INTO topgamers VALUES(null,'$_POST[txtNombre]','$destino','$_POST[txtgenero]','$_POST[descripcion]','$_POST[caracteristicas]','$_POST[txtnumerogamer]','$_POST[txtconsola]','$_POST[fecha]')");
	
	echo "<script type='text/javascript'>			
			window.location='adminListaTopGamers.php?';
		</script>";
}else
{
	?>

<div id="contenido">
<?php
	include('adminpaginadorTop.php');	
}
?>
</div>

  <!-- fin del programa y ejeuccion -->
            <div id="red_registro" style="display:none;">
              <?php include("form_modal.php"); ?>
           </div>

            <div id="red_space" style="height:10px; width:960px; float:left;"></div>

         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'http://www.desarrolloweb.com/articulos/ejemplos/jquery/ui/datepicker/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
<?php
}else{
  echo "<script type='text/javascript'>
        //alert('No está logueado');
        window.location='index.php';
        </script>";
}
?>