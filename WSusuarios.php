<?php

// WEB SERVICE para obtener los nombres de los usuarios que se muestran en la lista de sugerencias

require_once("class/class.php");

  	// realizamos la consulta de busqueda por nombre y apellido que contenga lo que venga via GET
	// mediante la variable 'term' que es usaba por la funcion autocomplete de JqueryUI
    $sql = "SELECT idUsuario, concat(nombres,' ',apellidos) Nombre, foto FROM usuarios 
            WHERE idUsuario <> '".$_SESSION['sesion_id']."' 
            AND (nombres LIKE '%".$_GET['term']."%' OR apellidos LIKE '%".$_GET['term']."%' OR correo LIKE '%".$_GET['term']."%') ";//consulta para seleccionar las palabras a buscar
    
    // ejecutamos la consulta
    $res = mysql_query( $sql,Conectar::con() );

    // guardamos la cantidad de filas devueltas
    $contador = mysql_num_rows( $res );

    // verificamos si cantidad es mayor a cero se crea un JSON para mostrar los registros
    if( $contador > 0 ){

        // creamos un array donde se guardaran los datos
        $return_arr = array();

        // recorremos lo que nos devuelve la consulta y definimos que sea un array asociativo
        while( $row = mysql_fetch_array( $res, MYSQL_ASSOC ) ){
           // if( $row['idUsuario'] != $_SESSION["sesion_id"] ){
            	$row_array['id'] = base64_encode($row['idUsuario']); // se guarda el id del usuario
            	$row_array['nombre'] = $row['Nombre']; // se guarda el nombre del usuario
                $row_array['foto'] = $row['foto']; // se guarda el nombre del usuario
            	$row_array['label'] = $row['Nombre']; // se guarda de nuevo el nombre usuario; este sirve para mostrar en la lista de sugerencias
            //}
        	// funcion de PHP para poder guardar varios elementos en un array de $row_array -> $return_arr
        	array_push( $return_arr, $row_array );
        }


        // se imprime el array con los datos y lo convertimos en un json para poder
        // interactuar con javascrit para mayor eficiencia
        echo json_encode( $return_arr );
    }
    else{ // si no hay registros que coincidan con lo buscado

        // creamos un array para guardar solo un texto de advertencia
        $return_arr = array();
        
        // agregamos un elemento en otro array para luego agregarlo al anterior
        $row_array['label'] = "No hay resultados";
        $row_array['foto'] = "triste.png";

        // agregamos al array lo que se mostrara en pantalla el error
        array_push( $return_arr, $row_array );

        // convertimos en JSON e imprimimos el array para mostrar
        echo json_encode( $return_arr );
    }

?>