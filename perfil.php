<?php
require_once('class/clases.php');
require_once('class/funciones.php');

if( isset($_SESSION["sesion_usuario"]) && isset($_SESSION["sesion_id"]))
{  
  $objUsuario = new Socialnet();
  $reg = $objUsuario->datos_amigo( base64_decode($_GET['user'] ) ) ;
  $amigo = $objUsuario->amigo( base64_decode($_GET['user'] ) );  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Perfil</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>      
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
      <script src="js/buscar.js"></script>     

      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>      

   </head>
   <body>
    	<div id="red_content">
        	<?php include("header.php");?>
          <div id="red_space" style="height:10px; width:960px;"></div>

         <div id="red_page" class="centrar">
            <div id="red_space" style="height:10px; width:960px;"></div>

            <div id="red_usuario_datos" class="centrar">
              <div id="red_usuario_foto">
                <?php 
                  if( $reg[0]['foto'] == '0' ){
                    $foto = "default.png";
                  }else{
                    $foto = $reg[0]['foto'];
                  }
                ?>
                  <img src="fotos_usuarios/<?php echo $foto; ?>">
              </div>
              <div id="red_usuario_nombre">
                <?php echo $reg[0]['nombre'];?>
              </div>
              <div id="red_usuario_personales">
                <?php 
                    if( $reg[0]['verFechaNac'] == true ){
                      echo "Fecha de nacimiento: ".cambiafechanormal($reg[0]['fechaNac']);
                      echo"<br>"; 
                    }
                  ?>
                
                <?php 
                  if( $reg[0]['verCorreo'] == true ){
                    echo $reg[0]['correo']; 
                  }
                ?>
              </div>
              <div id="red_usuario_agregar">
                <?php if( $amigo == 0 ){ ?>
                  <span><a href="agregaramigo.php?user=<?php echo $_GET['user'];?>">Agregar amigo</a></span>
                <?php }else if( $amigo == 1){ ?>
                  <span>Amigo</span>
                <?php }else if( $amigo == 2){ ?>
                  <span>Solicitud enviada</span>
                <?php }?>
              </div>
            </div>

            <div id="red_space" style="height:10px; width:960px;"></div>            

            <p>&nbsp;</p>
            <div id="red_lateral">
              <ul>
                <li><a href="miperfil.php" title="Mi perfil"><img src="img/perfil.png"> Mi perfil</a></li>                
                <li><a href="salir.php"><img src="img/salir.png"> Cerrar sesión</a></li>                
              </ul>              
            </div>            

            <div id="red_usuario_contenido">
              <div id="red_space" style="height:10px; width:960px; float:left;"></div>
              <div id="ruc_comentario">
                <form id="formRegistro" name="formRegistro" action="guardarComentario.php" method="post" autocomplete="off" >  
                   <div style="height: 170px; width:410px; " class="centrar" >
                      <div style="width:100px; height:130px; float:left;"  >Comentario:</div>
                      <div style="float:left; width:300px; height:130px; display:inline-block;" >
                         <textarea class="redondear" name="nombres" id="nombres" placeholder="Deja tu comentario" cols="40" rows="6" style="resize: none;"
                            pattern="[a-zA-Z]+" 
                            oninvalid="setCustomValidity('No válido')"
                            onchange="try{setCustomValidity('')}catch(e){}" required></textarea>
                      </div>
                      <div style="width:100px; height:25px; float:right;margin-top:15px;" class="centrar"  ><input type="submit" name="registrarse" id="registrarse" value="Registrarse" ></div>
                   </div>                   
                </form>
              </div>
              <div id="red_space" style="height:10px; width:960px; float:left;"></div>


                
              <div id="red_space" style="height:10px; width:960px; float:left;"></div>
            </div>

            <div id="red_registro" style="display:none;">
              <?php include("form_modal.php"); ?>
            </div>

            <div id="red_space" style="height:10px; width:960px; float:left;"></div>

         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'http://www.desarrolloweb.com/articulos/ejemplos/jquery/ui/datepicker/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
<?php
}else{
  echo "<script type='text/javascript'>
        //alert('No está logueado');
        window.location='index.php';
        </script>";
}
?>