<?php
require_once('class/clases.php');
require_once('class/funciones.php');

if( isset($_SESSION["sesion_usuario"]) && isset($_SESSION["sesion_id"]))
{  
  $objUsuario = new Socialnet();
  $reg = $objUsuario->datos_usuario();
  $notificaciones = $objUsuario->notificaciones();  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Mi Perfil</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
      <!--<link type="text/css" href="css/jquery-ui-1.8.1.custom.css" rel="Stylesheet" />      -->
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <!--<script src="js/vendor/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.1.custom.min.js"></script>-->
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
      <script src="js/buscar.js"></script>   
      <script type="text/javascript" src="clienajaxconsola.js"></script> 
           <script language="JavaScript" src="jquery-1.5.1.min.js"></script>
      <script language="JavaScript" src="jquery.watermarkinput.js"></script>
      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>      
<script type="text/javascript">
$(document).ready(function(){

$(".busca").keyup(function() //se crea la funcioin keyup
{
var texto = $(this).val();//se recupera el valor de la caja de texto y se guarda en la variable texto
var dataString = 'palabra='+ texto;//se guarda en una variable nueva para posteriormente pasarla a search.php
if(texto=='')//si no tiene ningun valor la caja de texto no realiza ninguna accion
{
}
else
{
$.ajax({//metodo ajax
type: "POST",//aqui puede  ser get o post
url: "search2.php",//la url adonde se va a mandar la cadena a buscar
data: dataString,
cache: false,
success: function(html)//funcion que se activa al recibir un dato
{
$("#display").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
	}
});
}return false;    
});
});
jQuery(function($){//funcion jquery que muestra el mensaje "Buscar amigos..." en la caja de texto
   $("#caja_busqueda").Watermark("Buscar consolas...");
   });
</script>
   </head>
   <body>
    	<div id="red_content">
        	<?php include("header.php");?>
          <div id="red_space" style="height:10px; width:950px;"></div>

         <div id="red_page" class="centrar">
            <div id="red_space" style="height:10px; width:960px;"></div>

            <div id="red_usuario_datos" class="centrar">
              <div id="red_usuario_nombre">
                <?php echo $reg[0]['nombre'];?>
              </div>
                          <div id="DisCLIENTE">
             <p>CLIENTE</p>
             </div>
            </div>
             <div id="cajon1"><li ><a href="clienListaGamers.php"><img src="img/perfil.png"> LISTA DE GAMERS</a></li>
                </div> 
                
                <div id="cajon2"><li><a href="clienListaTopGamers.php"><img src="img/logo_16.png"> TOP 10 GAMERS</a></li>
         </div>
         
         <div id="cajon3"><li class="red_lateral_activo"><a href="javascript:void()"><img src="img/logo_16.png"> TIPOS DE CONSOLAS</a></li>
         </div>
         
         <div id="cajon4"><li><a href="clienHistoriaGamers.php"><img src="img/logo_16.png"> HISTORIA DEL GAMERS</a></li>
         </div>

 <div id = "red_deregistro" align="center"><h1><p>LISTA DE CONSOLAS DE XBOX Y PLAYSTATION</p></h1></div>         
         <div id="red_deregistro">
          <div style=" width:250px; padding-left:3px; ">
  <input type="text" class="busca" id="caja_busqueda" name="clave"/><br />
  <div id="display"></div>

<p>
</div> </div>
         <div id = "contenido">
            <?php 
			include ('clienpaginadorconsola.php');
?>
           </div>
           <div id="red_webservice">
                <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
                </div>
           

            <div id="red_registro" style="display:none;">
              <?php include("form_modal.php"); ?>
           </div>

            <div id="red_space" style="height:10px; width:960px; float:left;"></div>

         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'http://www.desarrolloweb.com/articulos/ejemplos/jquery/ui/datepicker/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
<?php
}else{
  echo "<script type='text/javascript'>
        //alert('No está logueado');
        window.location='index.php';
        </script>";
}
?>