<?php
require_once('class/clases.php');
require_once('class/funciones.php');

if( isset($_SESSION["sesion_usuario"]) && isset($_SESSION["sesion_id"]))
{  
  $objUsuario = new Socialnet();
  $reg = $objUsuario->datos_usuario();
  $notificaciones = $objUsuario->notificaciones();  
  $amigos = $objUsuario->amigos($_SESSION["sesion_id"]);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Mi Perfil</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
      <!--<link type="text/css" href="css/jquery-ui-1.8.1.custom.css" rel="Stylesheet" />      -->
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <!--<script src="js/vendor/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.1.custom.min.js"></script>-->
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
      <script src="js/buscar.js"></script>     

      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>      

   </head>
   <body>
    	<div id="red_content">
        	<?php include("adminheader.php");?>
          <div id="red_space" style="height:10px; width:960px;"></div>

         <div id="red_page" class="centrar">
            <div id="red_space" style="height:10px; width:960px;"></div>

            <div id="red_usuario_datos" class="centrar">
              <div id="red_usuario_foto">
                <?php 
                  if( $reg[0]['foto'] == '0' ){
                    $foto = "default.png";
                  }else{
                    $foto = $reg[0]['foto'];
                  }
                ?>
                  <img src="fotos_usuarios/<?php echo $foto; ?>">
              </div>
              <div id="red_usuario_nombre">
                <?php echo $reg[0]['nombre'];?>
              </div>
              <div id="red_usuario_personales">
                <?php 
                    if( $reg[0]['verFechaNac'] == true ){
                      echo "Fecha de nacimiento: ".cambiafechanormal($reg[0]['fechaNac']);
                      echo"<br>"; 
                    }
                  ?>
                
                <?php 
                  if( $reg[0]['verCorreo'] == true ){
                    echo $reg[0]['correo']; 
                  }
                ?>
              </div>
            </div>

            <div id="red_space" style="height:10px; width:960px;"></div>            

            <div id="DisADMINISTRADOR">
                <p>ADMINISTRADOR </p>
            </div>
            <div id="red_lateral">
              <ul>
                <li><a href="adminMiperfil.php"><img src="img/perfil.png"> Perfil</a></li>
                <li class="red_lateral_activo"><a href="javascript:void()"><img src="img/logo_16.png"> Amigos</a></li>
                <li><a href="adminListaGamers.php"><img src="img/foro.png">Foro</a></li>
                               <li><a href="adminListaCliente.php"><img src="img/logo_16.png"> Lista Cliente</a></li>
                <li>
                  <a href="adminNotificaciones.php"><img src="img/notificacion.png"> Notificaciones 
                    <?php if( $notificaciones > 0 ){ ?>
                      <div class="circulo"><span><?php echo $notificaciones; ?></span> </div> 
                    <?php  }?>                      
                  </a>
                </li>
                <li><a href="adminConfiguracion.php"><img src="img/configuracion.png"> Configurar cuenta</a></li>
                               
              </ul>              
            </div>            

            <div id="red_usuario_contenido">
              <div class="centrar" style="width:300px; text-align:center;"><h2>Mis Amigos</h2></div>              
              <div id="ruc_panel_amigos" class="centrar">                                
                <div id="red_space" style="height:5px; width:960px;"></div>            
                <?php for( $i=0; $i< sizeof($amigos); $i++ ){ ?>
                  <div class="ruc_amigos">
                    <div class="ruc_amigo_foto">
                      <a href="perfil.php?user=<?php echo base64_encode($amigos[$i]['idUsuario']) ?>"><img src="fotos_usuarios/<?php echo $amigos[$i]['foto'] ?>" height="150" width="150" title="<?php echo $amigos[$i]['nombre'] ?>"></a>
                    </div>
                  </div>
                  <?php } ?>
              </div>
              <div id="red_space" style="height:10px; width:960px; float:left;"></div>
            </div>

            <div id="red_registro" style="display:none;">
              <?php include("form_modal.php"); ?>
            </div>

            <div id="red_space" style="height:10px; width:960px; float:left;"></div>

         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'http://www.desarrolloweb.com/articulos/ejemplos/jquery/ui/datepicker/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
<?php
}else{
  echo "<script type='text/javascript'>
        //alert('No está logueado');
        window.location='index.php';
        </script>";
}
?>