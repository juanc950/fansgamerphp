<?php
require_once('class/clases.php');
require_once('class/funciones.php');

if( isset($_SESSION["sesion_usuario"]) && isset($_SESSION["sesion_id"]))
{  
  $objUsuario = new Socialnet();
  $reg = $objUsuario->datos_usuario();
  $notificaciones = $objUsuario->notificaciones();  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>Mi Perfil</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width">

      <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/main.css">
      <link href='http://fonts.googleapis.com/css?family=Inder' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
      <!--<link type="text/css" href="css/jquery-ui-1.8.1.custom.css" rel="Stylesheet" />      -->
      <script src="js/vendor/modernizr-2.6.2.min.js"></script>
      <!--<script src="js/vendor/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui-1.8.1.custom.min.js"></script>-->
      <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
      <script src="js/buscar.js"></script>     

      <style type="text/css" >      
        @import "media/themes/smoothness/jquery-ui-1.9.2.custom.css";
      </style>      

   </head>
   <body>
    	<div id="red_content">
        	<?php include("header.php");?>
          <div id="red_space" style="height:10px; width:950px;"></div>

         <div id="red_page" class="centrar">
            <div id="red_space" style="height:10px; width:960px;"></div>

            <div id="red_usuario_datos" class="centrar">
              <div id="red_usuario_nombre">
                <?php echo $reg[0]['nombre'];?>
              </div>
                          <div id="DisCLIENTE">
             <p>CLIENTE</p>
             </div>
            </div>
            
                            <div id="cajon1"><li><a href="clienListaGamers.php"><img src="img/perfil.png"> LISTA DE GAMERS</a></li>
                </div> 
                
                <div id="cajon2"><li><a href="clienListaTopGamers.php"><img src="img/logo_16.png"> TOP 10 GAMERS</a></li>
         </div>
         
         <div id="cajon3"><li><a href="clienConsolas.php"><img src="img/logo_16.png"> TIPOS DE CONSOLAS</a></li>
         </div>
         
         <div id="cajon4"><li class="red_lateral_activo"><a href="javascript:void()"><img src="img/logo_16.png"> HISTORIA DEL GAMERS</a></li>
         </div>
             <div id = "red_deregistro" align="center"><h1><p>LA HISTORIA DE LOS VIDEOJUEGOS</p></h1></div>
            <div id="red_completa">
              <ul>
  <hr align="right" width="100%" color="blue">         

<div id="red_fotogam">
<img src="img/makinas.jpg" height="400" width="270"/></div>

<div class="sinopsis">La historia de los videojuegos tiene su origen en la década de 1940 cuando, tras el fin de la Segunda Guerra Mundial, las potencias vencedoras construyeron las primeras supercomputadoras programables como el ENIAC, de 1946. Los primeros intentos por implementar programas de carácter lúdico (inicialmente programas de ajedrez) no tardaron en aparecer, y se fueron repitiendo durante las siguientes décadas. Los primeros videojuegos modernos aparecieron en la década de los 60, y desde entonces el mundo de los videojuegos no ha dejado de crecer y desarrollarse con el único límite que le ha impuesto la creatividad de los desarrolladores y la evolución de la tecnología.
En los últimos años, se asiste a una era de progreso tecnológico dominada por una industria que promueve un modelo de consumo rápido donde las nuevas superproducciones quedan obsoletas en pocos meses, pero donde a la vez un grupo de personas e instituciones -conscientes del papel que los programas pioneros, las compañías que definieron el mercado y los grandes visionarios tuvieron en el desarrollo de dicha industria- han iniciado el estudio formal de la historia de los videojuegos.</div>
<p>&nbsp;</p>
<div class="sinopsis">El más inmediato reflejo de la popularidad que ha alcanzado el mundo de los videojuegos en las sociedades contemporáneas lo constituye una industria que da empleo a 120 000 personas y que genera unos beneficios multimillonarios que se incrementan año tras año. El impacto que supuso la aparición del mundo de los videojuegos significó una revolución cuyas implicaciones sociales, psicológicas y culturales constituyen el objeto de estudio de toda una nueva generación de investigadores sociales que están abordando el nuevo fenómeno desde una perspectiva interdisciplinar, haciendo uso de metodologías de investigación tan diversas como las específicas de la antropología cultural, la inteligencia artificial, la teoría de la comunicación, la economía o la estética, entre otras. Al igual que ocurriera con el cine y la televisión, el videojuego ha logrado alcanzar en apenas medio siglo de historia el estatus de medio artístico, y semejante logro no ha tenido lugar sin una transformación y evolución constante del concepto mismo de videojuego y de su aceptación. Nacido como un experimento en el ámbito académico, logró establecerse como un producto de consumo de masas en tan sólo diez años, ejerciendo un formidable impacto en las nuevas generaciones que veían los videojuegos con un novedoso medio audiovisual que les permitiría protagonizar en adelante sus propias historias.</div>
            
<hr align="right" width="100%" color="blue">         
              </ul>              
           </div>

           <div id="red_webservice">
                <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
                </div>
           
<!--finalizacion de la linea de codigo de coneccion a facebook --> 
            <div id="red_registro" style="display:none;">
              <?php include("form_modal.php"); ?>
           </div>

            <div id="red_space" style="height:10px; width:960px; float:left;"></div>

         </div>
         <?php include("footer.php") ?>
      </div>           

      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
      <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

      <script>
        var b=jQuery.noConflict();
        b(function() {
          b( "#fecha" ).datepicker({
            showOn: 'both',
            buttonImage: 'http://www.desarrolloweb.com/articulos/ejemplos/jquery/ui/datepicker/calendar.png',
            buttonImageOnly: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy'        
          });
        });     
      </script> 

      <script type="text/javascript" src="js/date.js"></script>
      
      <script src="js/prefixfree.min.js"></script>
		  <script src="js/plugins.js"></script>        
      <script src="js/main.js"></script>
   </body>
</html>
<?php
}else{
  echo "<script type='text/javascript'>
        //alert('No está logueado');
        window.location='index.php';
        </script>";
}
?>