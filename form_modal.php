<form id="formRegistro" name="formRegistro" action="registrarse.php" method="post" autocomplete="off" >  
   <div class="red_reg_campos">
      <div class="red_reg_label">Nombres:</div>
      <div class="red_reg_input">
         <input type="text" class="redondear" name="nombres" id="nombres" placeholder="Nombre" 
            pattern="([a-zA-Z]|\s)*" 
            oninvalid="setCustomValidity('No válido')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
      </div>
   </div>

   <div class="red_reg_campos">
      <div class="red_reg_label">Apellidos:</div>
      <div class="red_reg_input">
         <input type="text" class="redondear" name="apellidos" id="apellidos" placeholder="Apellidos" 
            pattern="([a-zA-Z]|\s)*" 
            oninvalid="setCustomValidity('No válido')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
      </div>
  </div>

   <div class="red_reg_campos">
      <div class="red_reg_label">Correo:</div>
      <div class="red_reg_input">
         <input type="email" class="redondear" name="correo" id="correo" placeholder="Correo"            
            oninvalid="setCustomValidity('Correo no válido')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
         </div>
   </div>

   <div class="red_reg_campos">
      <div class="red_reg_label">Contraseña:</div>
      <div class="red_reg_input">
         <input type="password" class="redondear" name="contrasena" id="contrasena" placeholder="Contraseña"
            pattern="^[A-Za-z0-9_]{6,15}$" 
            oninvalid="setCustomValidity('6 caracteres mínimo')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
      </div>
   </div>

   <div class="red_reg_campos">
      <div class="red_reg_label">Fecha de nacimiento:</div>
      <div class="red_reg_input">
         <input type="text" class="redondear" name="fecha" id="fecha" placeholder="Fecha de nacimiento"
            pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" 
            oninvalid="setCustomValidity('Fecha no válida')"
            onchange="try{setCustomValidity('')}catch(e){}" required>
      </div>
   </div>

   <div id="red_reg_terminos">
      <div class="red_reg_label"> <a href="#">Acepto los términos y condiciones de uso </a></div>
      <div class="red_reg_input">
         <input class="reg_check" type="checkbox" onclick="javascript:validar(this);">
      </div>
   </div>

   <div class="red_reg_button"><input type="submit" name="registrarse" id="registrarse" value="Registrarse" disabled ></div>
</form>